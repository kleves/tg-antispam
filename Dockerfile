FROM ubuntu
RUN apt update && apt install tesseract-ocr tesseract-dev -y
ENTRYPOINT "[tesseract]"
